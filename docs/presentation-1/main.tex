%% SPDX-License-Identifier: GPL
\documentclass[ucs]{beamer}
\usepackage{graphics}
\usepackage{listings}
\usepackage{dejavu}

% \hypersetup{%
% colorlinks=true,
% linkcolor=green,
% filecolor=magenta,
% urlcolor=cyan,
% }

\mode<presentation>{%
  \usetheme{EastLansing}
  \useinnertheme{rectangles}
}

\newcommand*{\myhref}[3][]{\href{#2}{\color{#1}{#3}}}

\title{Golang OAuth 2.0 grant implementations}
\author{Mbock}
\date{\today}

\begin{document}
\lstset{%
  language=Go,
  basicstyle=\tiny,
  showstringspaces=false
}

\defverbatim[colored]\authcodeauthcodegrant{%
  \lstinputlisting[firstline=151, lastline=177]{../../internal/client/v1/handlers.go}
}

\defverbatim[colored]\authcodeaccesstokengrant{%
  \lstinputlisting[firstline=179, lastline=214]{../../internal/client/v1/handlers.go}
}

\defverbatim[colored]\resourceownergrant{%
  \lstinputlisting[firstline=127, lastline=149]{../../internal/client/v1/handlers.go}
}

\defverbatim[colored]\clientcredsgrant{%
  \lstinputlisting[firstline=109, lastline=125]{../../internal/client/v1/handlers.go}
}

\begin{frame}
  \maketitle
\end{frame}

\begin{frame}
  \frametitle{Outline}
  \tableofcontents
\end{frame}

\section{Setup}

\begin{frame}
  A persistent store (PostgreSQL database) is required to facilitate the use
  of multiple authorization server instances; this also allows for recovery
  without reissuing tokens for all grant types.
\end{frame}

\begin{frame}
  \frametitle{Token store}

  The token store (table: oauth2\_tokens) will retain valid authorization
  codes, access tokens \& refresh tokens \& require the following attributes
  for
  \myhref[blue]{https://github.com/go-oauth2/oauth2}{github.com/go-oauth2/oauth2}:

  \begin{enumerate}
    \item id: bigserial
    \item created\_at: timestampz
    \item expires\_at: timestampz
    \item code: text
    \item access: text
    \item refresh: text
    \item data: jsonb
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Client store}

  The client store (table: oauth2\_clients) will retain Client Credentials
  Grant information \& require the following attributes for
  \myhref[blue]{https://github.com/go-oauth2/oauth2}{github.com/go-oauth2/oauth2}:

  \begin{enumerate}
    \item id: text;
    \item secret: text;
    \item domain: text; and
    \item data: jsonb
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{User store}

  For the sample implementation, a user store (table: users) is required for
  the Resource Owner Password Credentals Grant with the following
  attributes:

  \begin{enumerate}
    \item Username: text,
    \item Password: text (password hash),
    \item Role: []text (mapped to scopes)
  \end{enumerate}
\end{frame}

\section{Three-legged authorization}

\begin{frame}[allowframebreaks]
  \frametitle{Authorization Code Grant (with PKCE)}

  \begin{definition}
    \myhref[blue]{https://tools.ietf.org/html/rfc6749\#section-1.3.1}{IETF| RFC 6749}
  \end{definition}

  \begin{enumerate}
    \item The authorization code is obtained by using an authorization
      server as an intermediary between the client and resource owner.
    \item The client directs the resource owner to an authorization server,
      which in turn directs the resource owner back to the client with the
      authorization code.
    \item The resource owner's credentials are never shared with the client.
    \item The authorization code provides the ability to authenticate the
      client, as well as the transmission of the access token directly to
      the client without passing it through the resource owner.
    \item \myhref[blue]{https://tools.ietf.org/html/rfc7636}{Proof Key for
      Code Exchange (PKCE)} is used to mitigate authorization code
      intercepts.
  \end{enumerate}
\end{frame}

\begin{frame}[shrink=1]
  \begin{figure}
    \includegraphics{sequence-diagrams/auth-code-grant.png}
    \caption{Authorization Code Grant (with PKCE) sequence diagram}
  \end{figure}
\end{frame}

\begin{frame}
  \begin{example}
    \authcodeauthcodegrant{}
    % \lstinputlisting[firstline=150, lastline=177]{../internal/client/handlers.go}
  \end{example}
\end{frame}

\begin{frame}[shrink=1]
  % \begin{example}
  \authcodeaccesstokengrant{}
  % \end{example}
\end{frame}

\section{Two-legged authorization}

\begin{frame}
  \frametitle{Resource Owner Password Credentials Grant}
  \begin{definition}
    \myhref[blue]{https://tools.ietf.org/html/rfc6749\#section-1.3.4}{IETF| RFC 6749}
  \end{definition}

  \begin{enumerate}
    \item The password credentials (username and password) are used to
      get an access token.
    \item The credentials are used when there is a high degree of trust
      between the resource owner and the client.
    \item The resource owner credentials are used for a single request and
      are exchanged for an access token.
    \item This grant type can remove the need for the client to store
      the resource owner credentials for future use, by exchanging the
      credentials with a long-lived access token or refresh token.
  \end{enumerate}
\end{frame}

\begin{frame}[shrink=1]
  \begin{figure}
    \includegraphics{sequence-diagrams/resource-owner-pass-creds-grant.png}
    \caption{Resource Owner Password Credentials Grant sequence diagram}
  \end{figure}
\end{frame}

\begin{frame}
  \begin{example}
    \resourceownergrant{}
  \end{example}
\end{frame}

\begin{frame}
  \frametitle{Client Credentials Grant}

  \begin{definition}
    \myhref[blue]{https://tools.ietf.org/html/rfc6749\#section-1.3.3}{IETF| RFC 6749}
  \end{definition}

  \begin{enumerate}
    \item The client credentials are used as when the authorization scope is
      limited to the protected resources under the control of the client.
    \item Also applies to protected resources previously arranged with the
      authorization server.
    \item Used when the client is acting on its own behalf (the client is
      also the resource owner) or is requesting access to protected
      resources based on arrange authorization.
  \end{enumerate}
\end{frame}

\begin{frame}[shrink=1]
  \begin{figure}
    \includegraphics{sequence-diagrams/client-creds-grant.png}
    \caption{Client Credentials Grant sequence diagram}
  \end{figure}
\end{frame}

\begin{frame}
  \begin{example}
    \clientcredsgrant{}
  \end{example}
\end{frame}

\section*{}

\begin{frame}
  \frametitle{Alternatives}

  Alternatives to rolling a custom implementation:

  \begin{enumerate}
    \item \myhref[blue]{https://github.com/ory/hydra}{Hydra}; OpenID
      Certified™ OpenID Connect provider and OAuth 2.0 server without identity
      management (signup, login, password reset).
    \item \myhref[blue]{https://github.com/ory/kratos}{Kratos}; cloud native
      Identity and User Management System implementing OAuth 2.0, OpenID
      Connect, MFA, FIDO2, profile management, identity schemas, social sign
      in, registration, account recovery, service-to-service and IoT auth.
  \end{enumerate}
\end{frame}

\begin{frame}
  \frametitle{Miscellaneous}

  \begin{enumerate}
    \item Unused authorization flow
      \myhref[blue]{https://tools.ietf.org/html/rfc8628\#section-3.4}{IETF|
      OAuth 2.0 Device Access Token Request}
    \item Library implementation accompanying access tokens
      \myhref[blue]{https://tools.ietf.org/html/rfc6749\#section-1.5}{IETF|
      OAuth 2.0 Refresh Token}
    \item The WIP authorization server implementation utilizing
      \myhref[blue]{https://github.com/ory/fosite}{github.com/ory/fosite}
      implements additional security measures as discussed in
      \myhref[blue]{https://tools.ietf.org/html/rfc6819}{IETF| OAuth 2.0
      Threat Model…}
  \end{enumerate}
\end{frame}

\end{document}
