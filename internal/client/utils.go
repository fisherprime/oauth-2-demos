package client

import (
	"context"
	"crypto/sha256"
	"crypto/tls"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/go-chi/chi"
)

const (
	// codeVerifierAlphabet contains the alphabet used in generating the PKCE code verifier.
	codeVerifierAlphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~"
)

var (
	// codeVerifierLimits defines the lower & upper length limits for a PKCE code verifier.
	codeVerifierLimits = [2]int{43, 128}

	// codeVerifier random string used to verify an OAuth 2.0 client.
	codeVerifier string
)

// cliTestClient retrieves a response from the authorization test URL & returns the response.
func clientAuthTest(client *http.Client) (content []byte, err error) {
	var req *http.Request

	if req, err = http.NewRequestWithContext(context.Background(), "GET", cfg.Endpoint.Test, nil); err != nil {
		return
	}

	resp, err := client.Do(req)
	if err != nil {
		err = fmt.Errorf("unable to retrieve the test URL's response: %w", err)
		return
	}

	content, err = ioutil.ReadAll(resp.Body)
	_ = resp.Body.Close()
	if err != nil {
		err = fmt.Errorf("unable to read the test URL's response: %w", err)
		return
	}

	return
}

// generateRandString generates a random string of some arbitrary length.
func generateRandString(strLen int) string {
	strBuilder := strings.Builder{}
	strBuilder.Grow(strLen)

	lenAlphabet := len(codeVerifierAlphabet)

	for iter := strLen - 1; iter >= 0; iter-- {
		// nolint: gosec
		strBuilder.WriteByte(codeVerifierAlphabet[rand.Intn(lenAlphabet)])
	}

	return strBuilder.String()
}

// generateCodeChallenge Generates a PKCE code challenge: a base64 encoded hash (sha256) of a code
// verifier (random string).
//
// REF: https://www.oauth.com/oauth2-servers/pkce/authorization-request.
// REF: https://stackoverflow.com/a/31832326.
func generateCodeChallenge() (codeChallenge string, err error) {
	// nolint: gosec
	codeVerifierLength := codeVerifierLimits[0] + rand.Intn(codeVerifierLimits[1]-codeVerifierLimits[0])
	codeVerifier = generateRandString(codeVerifierLength)

	hash := sha256.New()
	if _, err = hash.Write([]byte(codeVerifier)); err != nil {
		err = fmt.Errorf("unable to hash the code verifier: %w", err)
		return
	}
	codeChallenge = base64.URLEncoding.EncodeToString(hash.Sum(nil))

	return
}

// redirectTo replies to a request with a redirect URL specified in the "Location" header.
func redirectTo(writer http.ResponseWriter, path string) {
	writer.Header().Set("Location", path)
	writer.WriteHeader(http.StatusFound)

	// NOTE: If the path specified is not absolute, it is taken a relative.
	// http.Redirect(writer, nil, path, http.StatusFound)
}

// configureHTTPService creates a `http.Server` instance with optional TLS v1.3 security.
func configureHTTPService(router *chi.Mux) (srv *http.Server, err error) {
	var tlsCfg *tls.Config

	if cfg.TLS.CertfilePath != "" && cfg.TLS.KeyfilePath != "" {
		var (
			certPath string
			keyPath  string
			cert     tls.Certificate
		)

		certPath, err = filepath.Abs(cfg.TLS.CertfilePath)
		if err != nil {
			err = fmt.Errorf("could not obtain absolute TLS certfile path:  %v", err)
			return
		}
		keyPath, err = filepath.Abs(cfg.TLS.KeyfilePath)
		if err != nil {
			err = fmt.Errorf("could not obtain absolute TLS keyfile path:  %v", err)
			return
		}

		cert, err = tls.LoadX509KeyPair(certPath, keyPath)
		if err != nil {
			err = fmt.Errorf("could not load TLS certfile & keyfile:  %v", err)
			return
		}

		tlsCfg = &tls.Config{
			Certificates: []tls.Certificate{cert},
			MinVersion:   tls.VersionTLS13,
		}
	}

	srv = &http.Server{
		Addr:         fmt.Sprintf("%s:%d", cfg.Runtime.ServiceHost, cfg.Runtime.ServicePort),
		Handler:      router,
		ReadTimeout:  cfg.Runtime.RequestReadTimeout,
		WriteTimeout: cfg.Runtime.ResponseWriteTimeout,
		TLSConfig:    tlsCfg,
	}

	return
}

// OverwriteFile overwrites the contents of a file with the supplied data.
func OverwriteFile(path string, data []byte) (err error) {
	var (
		outputPath string
		file       *os.File
	)

	outputPath, err = filepath.Abs(path)
	if err != nil {
		return
	}

	// nolint: gosec // "G304" is sorted out by `filepath.Abs`.
	file, err = os.OpenFile(outputPath, os.O_CREATE|os.O_RDWR, 0600)
	if err != nil {
		err = fmt.Errorf("failed to open file %s: %v", outputPath, err)
		return
	}

	_, err = file.Write(data)

	return
}
