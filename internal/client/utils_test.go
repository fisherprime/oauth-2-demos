package client

import (
	"testing"
)

func Test_generateRandString(t *testing.T) {
	type args struct {
		strLen int
	}

	tests := []struct {
		name    string
		args    args
		wantLen int
		unwant  string
	}{
		{
			name: "random len32",
			args: args{
				strLen: 32,
			},
			wantLen: 32,
		},
		{
			name: "random len24",
			args: args{
				strLen: 24,
			},
			wantLen: 24,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := generateRandString(tt.args.strLen)
			lenGot := len(got)

			if lenGot < 1 {
				t.Errorf("generateRandString() = \"\"")
			}

			if lenGot != tt.wantLen {
				t.Errorf("generateRandString() = %s, len = %d, wantLen=%v", got, lenGot, tt.wantLen)
			}
		})
	}
}

func Test_generateCodeChallenge(t *testing.T) {
	randChallenge, err := generateCodeChallenge()
	if err != nil {
		t.Errorf("generateCodeChallenge() for randomness error = %v", err)
	}

	tests := []struct {
		name                    string
		wantCodeChallengeLimits [2]int
		unWantCodeChallenge     string
		wantErr                 bool
	}{
		{
			name:                    "empty test",
			wantCodeChallengeLimits: codeVerifierLimits,
			unWantCodeChallenge:     "",
			wantErr:                 false,
		},
		{
			name:                    "randomnness",
			wantCodeChallengeLimits: codeVerifierLimits,
			unWantCodeChallenge:     randChallenge,
			wantErr:                 false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotCodeChallenge, err := generateCodeChallenge()
			if (err != nil) != tt.wantErr {
				t.Errorf("generateCodeChallenge() error = %s, wantErr %v", err.Error(), tt.wantErr)
				return
			}
			lenGot := len(gotCodeChallenge)

			if lenGot < 1 {
				t.Errorf("generateCodeChallenge() = \"\"")
			}

			if gotCodeChallenge == tt.unWantCodeChallenge {
				t.Errorf("generateCodeChallenge() = %s, unwanted %s, is not random", gotCodeChallenge, tt.unWantCodeChallenge)
			}

			if lenGot < tt.wantCodeChallengeLimits[0] || lenGot > tt.wantCodeChallengeLimits[1] {
				t.Errorf("generateCodeChallenge() = %s, len = %d, exceeds the length limits %d - %d",
					gotCodeChallenge, lenGot, tt.wantCodeChallengeLimits[0], tt.wantCodeChallengeLimits[1])
			}
		})
	}
}
