// Package serverv1 implements a sample OAuth 2.0 authorization server.

// The following grants are implemented: Authorization Code, Client Credentials & Resource Owner
// Password Credentials grants.
//
// Implemented with: https://github.com/go-oauth2/oauth2 &
// https://github.com/vgarvardt/go-oauth2-pg.
package server

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-oauth2/oauth2/v4"
	"github.com/go-oauth2/oauth2/v4/errors"
	"github.com/go-oauth2/oauth2/v4/manage"
	"github.com/go-oauth2/oauth2/v4/server"
	"github.com/jackc/pgx/v4/pgxpool"
	pg "github.com/vgarvardt/go-oauth2-pg/v4"
	"github.com/vgarvardt/go-pg-adapter/pgx4adapter"
)

var (
	cfg Config

	// dbPool is the PostgreSQL connection pool used for this package.
	dbPool *pgxpool.Pool

	// oauth2Server is the OAuth 2.0 Authorization Server used in this package.
	oauth2Server *server.Server
)

// StartServerV1 initiates an OAuth 2.0 authorization server instance.
func StartServer(c *Config) (err error) {
	const (
		// tokenGCInterval defines the time between garbage collection operations on the token
		// store.
		tokenGCInterval = time.Minute * 5
	)

	cfg = *c

	poolConfig, err := pgxpool.ParseConfig(cfg.Runtime.DatabaseURL)
	if err != nil {
		err = fmt.Errorf("unable to parse the database config:  %v", err)
		return
	}
	dbPool, err := pgxpool.ConnectConfig(context.Background(), poolConfig)
	if err != nil {
		err = fmt.Errorf("unable to connect to the database:  %v", err)
		return
	}
	defer dbPool.Close()

	// Use PostgreSQL token store with pgx.Connection adapter.
	dbConn, err := dbPool.Acquire(context.TODO())
	if err != nil {
		err = fmt.Errorf("unable to acquire PostgreSQL connection:  %v", err)
		return
	}
	adapter := pgx4adapter.NewConn(dbConn.Conn())

	// Where the credentials of known clients is stored.
	//
	// NOTE: The clientStore table is `oauth2_clients`.
	clientStore, err := pg.NewClientStore(adapter)
	if err != nil {
		err = fmt.Errorf("unable to create a PostgreSQL client store:  %v", err)
		return
	}

	// Where the authorization server generated tokens are stored.
	//
	// NOTE: The tokenStore table is `oauth2_tokens`.
	tokenStore, err := pg.NewTokenStore(adapter, pg.WithTokenStoreGCInterval(tokenGCInterval))
	if err != nil {
		err = fmt.Errorf("unable to create a PostgreSQL token store:  %v", err)
		return
	}
	defer tokenStore.Close()

	manager := manage.NewDefaultManager()
	manager.MapClientStorage(clientStore)
	manager.MapTokenStorage(tokenStore)

	router := chi.NewRouter()
	router.Use(middleware.Logger, middleware.Recoverer)

	srv, err := configureServer(router, manager)
	if err != nil {
		return
	}

	idleConnsClosed := make(chan struct{})
	shutdownService(idleConnsClosed, srv)

	if srv.TLSConfig != nil {
		err = srv.ListenAndServeTLS("", "")
	} else {
		err = srv.ListenAndServe()
	}

	if err != http.ErrServerClosed {
		return
	}
	err = nil

	<-idleConnsClosed

	return
}

// shutdownService allows for graceful shutdown of the HTTP service.
func shutdownService(idleConnsClosed chan struct{}, srv *http.Server) {
	sigint := make(chan os.Signal, 1)
	signal.Notify(sigint, os.Interrupt)
	<-sigint

	// We received an interrupt signal, shut down.
	if err := srv.Shutdown(context.Background()); err != nil {
		// Error from closing listeners, or context timeout:
		log.Printf("HTTP service Shutdown: %v", err)
	}
	close(idleConnsClosed)
}

// configureServer configures the OAuth 2.0 authorization server's handlers.
func configureServer(router *chi.Mux, manager *manage.Manager) (srv *http.Server, err error) {
	// Set the authorization `code` expiry to 60 seconds.
	manager.SetAuthorizeCodeExp(cfg.Runtime.AuthCodeExp)

	// Configure the Authorization Code Grant.
	manager.SetAuthorizeCodeTokenCfg(&manage.Config{
		AccessTokenExp:    cfg.Runtime.AccessTokenExp,
		RefreshTokenExp:   cfg.Runtime.AuthCodeRefreshTokenExp,
		IsGenerateRefresh: true,
	})

	// Configure the Resource Owner Password Credentials Grant.
	manager.SetPasswordTokenCfg(&manage.Config{
		AccessTokenExp:    cfg.Runtime.AccessTokenExp,
		RefreshTokenExp:   cfg.Runtime.PassCredsRefreshTokenExp,
		IsGenerateRefresh: true,
	})

	// Configure the Client Credentials Grant.
	// NOTE: A refresh token is not necessary for a Client Credentials Grant client (a Resource
	// Owner).
	manager.SetClientTokenCfg(&manage.Config{
		AccessTokenExp: cfg.Runtime.AccessTokenExp,
	})

	oauth2Server = server.NewDefaultServer(manager)

	oauth2Server.SetAllowGetAccessRequest(true)
	oauth2Server.SetAllowedGrantType(oauth2.AuthorizationCode, oauth2.ClientCredentials,
		oauth2.PasswordCredentials, oauth2.Refreshing)
	oauth2Server.SetClientInfoHandler(server.ClientFormHandler)
	oauth2Server.SetPasswordAuthorizationHandler(passCredsAuthHandler)
	oauth2Server.SetUserAuthorizationHandler(userAuthenticator)

	oauth2Server.SetInternalErrorHandler(func(err error) (resp *errors.Response) {
		log.Println("Internal Error: ", err)
		return
	})
	oauth2Server.SetResponseErrorHandler(func(resp *errors.Response) {
		log.Println("Response Error: ", resp.Error)
	})

	// The authorization route.
	router.Get("/authorize", func(wr http.ResponseWriter, req *http.Request) {
		err := oauth2Server.HandleAuthorizeRequest(wr, req)
		if err != nil {
			http.Error(wr, err.Error(), http.StatusBadRequest)
		}
	})

	// The token generation route.
	router.Get("/token", func(wr http.ResponseWriter, req *http.Request) {
		err := oauth2Server.HandleTokenRequest(wr, req)
		if err != nil {
			http.Error(wr, err.Error(), http.StatusBadRequest)
		}
	})

	// The test page route.
	router.Get("/test", testHandler)

	return configureHTTPService(router)
}
