package server

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

// passAuthenticator validates a user supplied username-password pair.  On validation, the userID is
// set.
func passCredsAuthHandler(username, password string) (userID string, err error) {
	var role string

	dbConn, err := dbPool.Acquire(context.TODO())
	if err != nil {
		err = fmt.Errorf("unable to acquire PostgreSQL connection: %w", err)
		return
	}
	if _, err = dbConn.Conn().Prepare(context.Background(), "userPassQuery",
		"select userid, role from users where username=$1::string and password=$2::string"); err != nil {
		err = fmt.Errorf("could not prepare username-password query: %w", err)
		return
	}
	if err = dbPool.QueryRow(context.Background(), "userPassQuery", username,
		password).Scan(&userID, &role); err != nil {
		err = fmt.Errorf("could not authorize user: %w", err)
		return
	}

	return
}

// userAuthenticator validates a user supplied access token.
//
// On validation, the request is processed.
// On failure, the user is redirected to the login page.
func userAuthenticator(wr http.ResponseWriter, req *http.Request) (userID string, err error) {
	token, err := oauth2Server.ValidationBearerToken(req)
	if err != nil {
		wr.Header().Set("Location", "/login")
		wr.WriteHeader(http.StatusFound)

		return
	}

	userID = token.GetUserID()

	return
}

// testHandler handles the "/test" route requests.
func testHandler(wr http.ResponseWriter, req *http.Request) {
	token, err := oauth2Server.ValidationBearerToken(req)
	if err != nil {
		http.Error(wr, err.Error(), http.StatusBadRequest)
		return
	}

	if err := req.ParseForm(); err != nil {
		http.Error(wr, err.Error(), http.StatusBadRequest)
		return
	}
	scope := []string{req.Form.Get("scope")}
	if scope == nil {
		http.Error(wr, "Undefined scope", http.StatusBadRequest)
	}

	/*     data := map[string]interface{}{
	 *         "expires_in": int64(token.GetAccessCreateAt().Add(token.GetAccessExpiresIn()).Sub(time.Now()).Seconds()),
	 *         "client_id":  token.GetClientID(),
	 *         "user_id":    token.GetUserID(),
	 *     }
	 *
	 *     encoder := json.NewEncoder(wr)
	 *     encoder.SetIndent("", "  ")
	 *     encoder.Encode(data) */

	output := map[string]interface{}{
		"token": token,
		"scope": scope,
	}

	encoder := json.NewEncoder(wr)
	encoder.SetIndent("", "  ")
	_ = encoder.Encode(output)
}
