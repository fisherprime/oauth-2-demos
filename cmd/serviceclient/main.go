// Package main provides the sample OAuth 2.0 microservice client binary.
package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/ilyakaznacheev/cleanenv"
	log "github.com/sirupsen/logrus"
	"gitlab.com/fisherprime/oauth-2-client-server-sample/internal/client"
	"gopkg.in/yaml.v3"
)

func main() {
	const configExample = "config-sample.yml"

	var (
		args struct {
			ConfigFile         string
			GenerateConfigFile bool
		}
		cfg client.Config
	)

	flags := flag.NewFlagSet("OAuth 2.0 HTTP client", flag.ExitOnError)
	flags.StringVar(&args.ConfigFile, "c", "configs/client/config.yml", "Path to configuration file")
	flags.BoolVar(&args.GenerateConfigFile, "g", false, fmt.Sprintf("Generate sample configuration file: %s", configExample))

	flagsUsage := flags.Usage
	flags.Usage = func() {
		flagsUsage()

		if descr, _ := cleanenv.GetDescription(&cfg, nil); descr != "" {
			fmt.Fprintln(flags.Output(), "\n", descr)
		}
	}

	if err := flags.Parse(os.Args[1:]); err != nil {
		log.Fatal(err)
	}

	if args.GenerateConfigFile {
		output, err := yaml.Marshal(client.NewDefaultConfig())
		if err != nil {
			log.Fatal(err)
		}

		if err := client.OverwriteFile(configExample, output); err != nil {
			log.Fatal(err)
		}

		return
	}

	if err := cleanenv.ReadConfig(args.ConfigFile, &cfg); err != nil {
		log.Fatal(err)
	}

	log.Fatal(client.StartHTTPClient(&cfg))
}
